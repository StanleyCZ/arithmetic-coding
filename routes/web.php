<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CompressionController@showHomepage');

Route::get('/encode', "CompressionController@showHomepage");
Route::get('/decode', "CompressionController@showHomepage");
Route::post('/encode', "CompressionController@encodeText");
Route::post('/decode', "CompressionController@decodeText");

