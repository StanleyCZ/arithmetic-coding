<?php
/**
 * Created by PhpStorm.
 * User: stana
 * Date: 25.11.2018
 * Time: 12:21
 */

namespace App\Http\Controllers;

use App\Http\Requests\DecodeFileRequest;
use App\Http\Requests\EncodeFileRequest;

/**
 * Class CompressionController
 *
 * Encoding and decoding a file using arithmetic coding
 *
 * @package App\Http\Controllers
 */
class CompressionController extends Controller
{
    /**
     * Redirect to 'welcome' page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View View for 'welcome' page.
     */
    public function showHomepage() {
        return view('welcome');
    }

    /**
     * Main block for encoding file.
     *
     * @param EncodeFileRequest $request Input file.
     * @return \Illuminate\Http\RedirectResponse View for 'result' page.
     */
    public function encodeText(EncodeFileRequest $request) {
        if ($request->file('inputFile')->getClientOriginalExtension() !== 'txt')
            return redirect('/')->with('error', 'Vložený soubor musí být ve formátu TXT!');

        $this->uploadFile($request->file('inputFile'), 'encode.txt');

        $originalText = $this->getOriginalFileText('encode.txt') . "#";
        $charTable = $this->getCharacterOccurrences($originalText);
        $result = $this->finalEncodeText($charTable, $originalText);
        $n = (end($result)['lowK'] + end($result)['highK']) / 2;
        $this->prepareAndSaveFile($n, $charTable);

        return view('result')
            ->with('steps', $result)
            ->with('n', $n)
            ->with("success", "Zakódování proběhlo úspěšně!");
    }

    /**
     * Sum of occurrences of the character and calculation of percentage representation.
     *
     * @param $originalText string Original text from input file.
     * @return mixed Return from getCharacterIntervals.
     */
    private function getCharacterOccurrences($originalText) {
        $charTable = array();
        $originalTextLength = strlen($originalText);

        for ($i = 0; $i < strlen($originalText); $i++) {
            if (array_key_exists($originalText[$i], $charTable)) {
                $charTable[$originalText[$i]]['count']++;
                $charTable[$originalText[$i]]['percentage'] = $charTable[$originalText[$i]]['count'] / $originalTextLength;
            } else {
                $charTable[$originalText[$i]]['count'] = 1;
                $charTable[$originalText[$i]]['percentage'] = $charTable[$originalText[$i]]['count'] / $originalTextLength;
            }
        }

        return $this->getCharacterIntervals($charTable);
    }

    /**
     * Calculation of the lower and upper bounds of the interval for each character.
     *
     * @param $charTable array Character table and their processed properties.
     * @return $charTable array Character table completed with the lower and upper bounds of the character interval.
     */
    private function getCharacterIntervals($charTable) {
        $intervalStart = 0;

        foreach ($charTable as $key => $occurrence) {
            $charTable[$key]['low'] = $intervalStart;
            $intervalStart += $charTable[$key]['percentage'];
            $charTable[$key]['high'] = $intervalStart;
        }

        return $charTable;
    }

    /**
     * Calculation of number 'n'.
     *
     * @param $charTable array Character table with it's properties.
     * @param $originalText string Original text from input file.
     * @return array Processed table with calculation steps.
     */
    private function finalEncodeText($charTable, $originalText) {
        $process = array(0 => array('char' => '_', 'rangeK-1' => 1, 'lowK' => 0, 'highK' => 1));

        for ($i = 0; $i < strlen($originalText); $i++) {
            $previousStep = end($process);

            $keys = array_keys($process);
            $step = end($keys) + 1;
            $char = $originalText[$i];
            $range = $previousStep['highK'] - $previousStep['lowK'];
            $high = $previousStep['lowK'] + $range * $charTable[$char]['high'];
            $low = $previousStep['lowK'] + $range * $charTable[$char]['low'];

            $process[$step] = array('char' => $char, 'rangeK-1' => $range, 'lowK' => $low, 'highK' => $high);
        }

        return $process;
    }

    /**
     * Encoding result preparation for save on the server and user download.
     *
     * @param $n float Output number.
     * @param $table array Table with character percentage representation.
     */
    private function prepareAndSaveFile($n, $table) {
        $fileContent = array('ni' => $n);

        foreach ($table as $charKey => $row) {
            $fileContent[$charKey] = $row['percentage'];
        }

        file_put_contents('files/encodedText.json', json_encode($fileContent));
    }

    /**
     * Main block for decoding file.
     *
     * @param DecodeFileRequest $request Input file.
     * @return \Illuminate\Http\RedirectResponse View for 'result' page.
     */
    public function decodeText(DecodeFileRequest $request) {
        if ($request->file('inputFile')->getClientOriginalExtension() !== 'json')
            return redirect('/')->with('error', 'Vložený soubor musí být ve formátu JSON!');

        $this->uploadFile($request->file('inputFile'), 'encodedText.json');
        $fileContent = json_decode(file_get_contents('files/encodedText.json'), true);

        $n = $fileContent['ni'];
        unset($fileContent['ni']);

        $fileContent = $this->reformatEncodedText($fileContent);
        $fileContent = $this->getCharacterIntervals($fileContent);
        $result = $this->finalDecodeText($n, $fileContent);

        file_put_contents('files/decodedText.txt', $result['originalText']);

        return view('result')
            ->with('steps', $result['decodedTable'])
            ->with('result', $result['originalText'])
            ->with('success', 'Dekódování proběhlo úspěšně!');
    }

    /**
     * Reformat of input array into workable form.
     *
     * @param $fileContent
     * @return mixed
     */
    private function reformatEncodedText($fileContent) {
        foreach ($fileContent as $key => $row) {
            $fileContent[$key] = array('percentage' => $row);
        }
        return $fileContent;
    }

    /**
     * Decoding text.
     *
     * @param $n float Initial calculation number.
     * @param $fileContent array Encoded file content.
     * @return $returnValue array Contains calculated original file text and all calculation steps.
     */
    private function finalDecodeText($n, $fileContent) {
        $decodedTable = array();
        $originalText = '';
        $i = -1;

        do {
            $i++;
            $decodedTable[$i] = array();

            if ($i === 0) {
                $decodedTable[$i]['n'] = $n;
            } else {
                $decodedTable[$i]['n'] = ($decodedTable[$i - 1]['n'] - $decodedTable[$i - 1]['low']) / $decodedTable[$i - 1]['range'];
            }
            $boundary = $this->getBoundary($decodedTable[$i]['n'], $fileContent);
            $decodedTable[$i]['low'] = $boundary['low'];
            $decodedTable[$i]['high'] = $boundary['high'];
            $decodedTable[$i]['range'] = $boundary['high'] - $boundary['low'];
            $decodedTable[$i]['char'] = $boundary['char'];

            if ($boundary['char'] !== '#') $originalText .= $decodedTable[$i]['char'];

        } while ($boundary['char'] !== '#');

        $returnValue['originalText'] = $originalText;
        $returnValue['decodedTable'] = $decodedTable;

        return $returnValue;
    }

    /**
     * Determining to which interval the calculated N belongs.
     *
     * @param $n float Calculated 'n'
     * @param $fileContent array Character table
     * @return $boundary array Array with low and high boundary and char.
     */
    private function getBoundary($n, $fileContent) {
        $boundary = array(
            'low' => 0,
            'high' => 0,
            'char' => ''
        );

        foreach ($fileContent as $key => $row) {
            if ($row['low'] < $n && $row['low'] > $boundary['low']) $boundary['low'] = $row['low'];
            if ($row['high'] > $n && $boundary['high'] === 0) {
                $boundary['high'] = $row['high'];
                $boundary['char'] = $key;
            }
        }

        return $boundary;
    }

    /**
     * Uploading the file to the server.
     *
     * @param $file file Uploaded file by user.
     * @param $fileName string Required storage name.
     */
    public function uploadFile($file, $fileName) {
        $file->move(public_path('/files'), $fileName);
    }

    /**
     * Loading file contents.
     *
     * @param $fileName string File name.
     * @return bool|string File content.
     */
    private function getOriginalFileText($fileName) {
        return file_get_contents('files/' . $fileName);
    }

    /**
     * Extraction of variable.
     *
     * For testing purposes.
     *
     * @param $description string Variable description.
     * @param null $variable mixed Variable to extract.
     */
    private function printVariable($description, $variable = null) {
        echo $description . ": " . $variable . "<br>";
    }

    /**
     * Extraction of arrays.
     *
     * For testing purposes.
     *
     * @param $description string Array description.
     * @param $array array Array to extract.
     */
    private function printArray($description, $array) {
        $this->printVariable($description);

        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
}