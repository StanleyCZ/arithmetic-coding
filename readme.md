## Arithmetic coding algorithm

This web application was created as part of a study at the Faculty of Business and Economics at the Mendel University in Brno.

Since the work was created for study and demonstration purposes only, it is able to decode only text of fifteen characters (approximately).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
