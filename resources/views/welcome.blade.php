@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md">
                <div class="card">
                    <div class="card-header text-white bg-dark">
                        Nástroj pro aritmetické kódování
                    </div>
                    <div class="card-body text-black bg-light">
                        @include('layouts.flash-messages')
                        <h5 class="card-title">Postup</h5>
                        <p class="card-text">Pokud potřebujete zakódovat text pomocí aritmetického kódování, vložte
                            soubor ve formátu TXT. V opačném případě vložte soubor vytvořený naší aplikací ve formátu
                            JSON.</p>
                        <div class="row">
                            <div class="col-md-6 border-right">
                                <form method="POST" action="{{ url("/encode") }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="text-center">
                                        <input type="file" class="btn btn-light form-control-file" id="inputFileForEncoding" name="inputFile">
                                    </div>
                                    <div class="text-center mt-3 mb-2">
                                        <button type="submit" class="btn btn-dark ">Zakódovat soubor
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6 border-left">
                                <form method="POST" action="{{ url("/decode") }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="text-center">
                                        <input type="file" class="btn btn-light form-control-file" id="inputFileForDecoding" name="inputFile">
                                    </div>
                                    <div class="text-center mt-3 mb-2">
                                        <button type="submit" class="btn btn-dark">Dekódovat soubor</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection