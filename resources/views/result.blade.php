@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md">
                <div class="card mb-5">
                    <div class="card-header text-white bg-dark">
                        Nástroj pro aritmetické kódování
                    </div>
                    <div class="card-body text-black bg-light pb-4">
                        @include('layouts.flash-messages')
                        <h5 class="card-title">Stáhněte @if(Request::segment(1) === 'encode')zakódovaný @else dekódovaný @endif soubor</h5>
                        <div class="row">
                            <div class="col-md-12">
                                @if(Request::segment(1) === 'encode')
                                    <div class="ml-5">
                                        <div class="row">
                                            <div class="col-md-1 mr-1 text-center text-white bg-dark rounded">Krok</div>
                                            <div class="col-md-1 mr-1 text-center text-white bg-dark rounded">Znak</div>
                                            <div class="col-md-3 mr-1 text-center text-white bg-dark rounded">RangeK-1
                                            </div>
                                            <div class="col-md-3 mr-1 text-center text-white bg-dark rounded">LowK</div>
                                            <div class="col-md-3 text-center text-white bg-dark rounded">HighK</div>
                                        </div>

                                        @foreach($steps as $step => $key)
                                            <div class="row text-center">
                                                <div class="col-md-1 mr-1">{{$step}}</div>
                                                <div class="col-md-1 mr-1">{{$key['char']}}</div>
                                                <div class="col-md-3 mr-1">{{$key['rangeK-1']}}</div>
                                                <div class="col-md-3 mr-1">{{$key['lowK']}}</div>
                                                <div class="col-md-3">{{$key['highK']}}</div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div style="margin: 0 auto" class="mt-4 mb-4 text-white bg-dark col-md-5 text-center rounded result">
                                        Výsledné „n“ je rovno = {{$n}}
                                    </div>
                                @else
                                    <div class="ml-5">
                                        <div class="row">
                                            <div class="col-md-2 mr-1 text-center text-white bg-dark rounded">Krok</div>
                                            <div class="col-md-2 mr-1 text-center text-white bg-dark rounded">N</div>
                                            <div class="col-md-2 mr-1 text-center text-white bg-dark rounded">Low</div>
                                            <div class="col-md-2 mr-1 text-center text-white bg-dark rounded">High</div>
                                            <div class="col-md-2 mr-1 text-center text-white bg-dark rounded">Range
                                            </div>
                                            <div class="col-md-1 text-center text-white bg-dark rounded">Znak</div>
                                        </div>

                                        @foreach($steps as $step => $key)
                                            <div class="row text-center">
                                                <div class="col-md-2 mr-1">{{$step}}</div>
                                                <div class="col-md-2 mr-1">{{round($key['n'], 10)}}</div>
                                                <div class="col-md-2 mr-1">{{round($key['low'], 20)}}</div>
                                                <div class="col-md-2 mr-1">{{round($key['high'], 20)}}</div>
                                                <div class="col-md-2 mr-1">{{round($key['range'], 20)}}</div>
                                                <div class="col-md-1">{{$key['char']}}</div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="mt-4 mb-4 text-white bg-dark col-md-5 text-center rounded result">
                                        Výsledný řetězec je „{{$result}}“
                                    </div>
                                @endif


                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                @if(Request::segment(1) === 'encode')
                                    <a class="btn btn-dark" href={{ asset('files/encodedText.json') }} download>Stáhnout
                                        soubor</a>
                                @else
                                    <a class="btn btn-dark" href={{ asset('files/decodedText.txt') }} download>Stáhnout
                                        soubor</a>
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <a class="btn btn-dark" href={{ url('/') }} >Přejít na úvodní stránku</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection